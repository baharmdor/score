Rails.application.routes.draw do
  get 'sessions/new'
  get 'scoresheets' => 'scoresheets#index'
  post 'scoresheets' => 'scoresheets#index'
  post 'scoresheets/:id' => 'scoresheets#edit'
  get 'scoresheets/:id' => 'scoresheets#edit'
  patch 'scoresheets/:id' => 'scoresheets#update'
  root 'static_pages#home'
  get '/static_pages/admin', to: 'static_pages#admin'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  resources :users
  resources :scoresheets,          only: [:index, :create, :destroy, :edit]
end
