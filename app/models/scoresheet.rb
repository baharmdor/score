class Scoresheet < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  validates :team, presence: true
end
