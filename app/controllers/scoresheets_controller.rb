class ScoresheetsController < ApplicationController
  before_action :logged_in_user, only: [:index, :create, :destroy, :edit, :show]
  before_action :correct_user, only: [:index, :edit, :show]
 
 def index
   @scoresheet = Scoresheet.new
 end

  def create
    @users=User.all
    @users.each do |user|
        @scoresheet = user.scoresheets.build(team_params)
       if @scoresheet.save
         flash[:success] = "Team created!"
       end
  end
    redirect_to 'static_pages/admin'
  end
  
def show
     render 'static_pages/test'
end

def destroy
end
  
def edit
  if current_user.admin?
        @scoresheet = Scoresheet.find_by(id: params[:id])
      else 
    @scoresheet = current_user.scoresheets.find(params[:id])
  end
end

  def update 
     @scoresheet = current_user.scoresheets.find(params[:id])
    if @scoresheet.update_attributes(scoresheet_params)
      flash[:success] = "Profile updated"
      redirect_to current_user
    else
      render 'edit'
    end
  end  
  
   private
   
    def scoresheet_params
      params.require(:scoresheet).permit(:team,:correctness,:creativity,:relevance,:sustainability,:user_experience)
    end
    
    def team_params
      params.require(:scoresheet).permit(:team)
    end
    
  def correct_user
      if current_user.admin?
        @scoresheet = Scoresheet.find_by(id: params[:id])
      else  
        @scoresheet = current_user.scoresheets.find_by(id: params[:id])
        redirect_to root_url if @scoresheet.nil?
      end  
  end
end
