require 'test_helper'

class ScoresheetTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @scoresheet = @user.scoresheets.build(team: 1)
  end

  test "should be valid" do
    assert @scoresheet.valid?
  end

  test "user id should be present" do
    @scoresheet.user_id = nil
    assert_not @scoresheet.valid?
  end
end
