class CreateScoresheets < ActiveRecord::Migration[5.0]
  def change
    create_table :scoresheets do |t|
      t.integer :team
      t.references :user, foreign_key: true
      t.integer :correctness
      t.integer :creativity
      t.integer :relevance
      t.integer :sustainability
      t.integer :user_experience

      t.timestamps
    end
  end
end
